import SocketUser_1 from './SocketUser.js'
import Queue_1 from './Queue.js'
import helper from './helper.js'

var Message = (function() {

	/** 手机震动 */
	function vibrateShort() {

		uni.vibrateShort({
			success: function() {}
		});

	}

	/** 向会话列表追加会话消息 */
	function PushMessage(v, msg, isOffline, isSend, data) {

		for (var i = 0; i < v.messages.length; i++) {
			var item = v.messages[i];
			if (item.key == msg.key) {
				v.messages.splice(i, 1);
				msg.count = msg.count + item.count;
			}
		}

		var chatmsg = helper.SesstionMsgForm(msg, data.userKey);

		switch (msg.type) {
			case 'img':
				msg.message = '[图片]';
				break;
			case 'web':
				msg.message = '[网址]';
				break;
			case 'voice':
				msg.message = '[语音]';
				break;
			case 'map':
				msg.message = '[位置]';
				break;
			case 'card':
				msg.message = '[信息卡片]';
				break;
			case 'product':
				msg.message = '[产品]';
				break;
			case 'order':
				msg.message = '[订单信息]';
				break;
			case 'redEnvelope':
				msg.message = '[红包]';
				break;
			case 'text':
			default:
				break;
		}

		if (data.parameter.title) {
			msg.message = msg.title + ':' + msg.message;
			msg.title = data.parameter.title;
		}

		if (data.parameter.url) {
			msg.url[0] = data.parameter.url;
		}

		v.messages.splice(0, 0, msg);

		if (!isOffline) {

			if (v.sesstion == msg.key.toString()) {
				msg.count = 0;
				v.setMsg(chatmsg);
			} else if (v.isHide) {
				plus.push.createMessage(msg.title + ':' + msg.message);
			} else {
				vibrateShort();
			}

		}

		if (data.userKey != v.userKey) {
			helper.SetChatMsg(chatmsg, msg.key);
		}


		helper.SetSesstion(v.messages);

	}

	/** 收到消息确认 */
	function ItemMessageConfirm(token) {
		var q = new Queue_1.Queue();
		q.Confirm(token);
	}

	/** 消息已读 */
	function ItemMessageFinished(token) {
		var q = new Queue_1.Queue();
		q.Finished(token);
	}

	function ItemMsessageForm(v, date, isOffline) {

		if (date.Token) {
			ItemMessageConfirm(date.Token);
			// ItemMessageFinished(date.Token);
		}


		var reg = new RegExp("'", "g");

		var userinfo = JSON.parse(date.userParameter.replace(reg, '"'));
		var parameter = JSON.parse(date.messageParameter.replace(reg, '"'));



		PushMessage(v, {
			title: userinfo.userName,
			url: [userinfo.headImg],
			message: date.message,
			time: helper.timeForm(date.sendDateTime),
			count: 1,
			stick: false,
			key: date.messageType == 'Group' ? date.MessageKey : date.userKey,
			type: parameter.type,
			len: parameter.length,
			parameter: parameter,
			disabled: false
		}, isOffline, false, {
			userKey: date.userKey,
			parameter: parameter,
			userinfo: userinfo
		});
	}

	function Message(v, type, date) {
		switch (type) {

			case 'clients':
				console.log('clients:' + date.length)
				break;
			case 'length':

				var datatime = new Date();

				var
					hours = datatime.getHours() + '',
					minutes = datatime.getMinutes() + '',
					seconds = datatime.getSeconds() + '';

				hours = (hours.length - 0) > 1 ? hours + '' : '0' + hours;
				minutes = minutes.length - 0 > 1 ? minutes + '' : '0' + minutes;
				seconds = seconds.length - 0 > 1 ? seconds + '' : '0' + seconds;

				v.heartbeattime = hours + ':' + minutes + ':' + seconds;

				break;
			case 'userlogin':
				console.log('userlogin:' + date)
				break;
			case 'offline':
				console.log('offline:' + date)
				date = JSON.parse('[' + date + ']');
				for (var i = 0; i < date.length; i++) {
					ItemMsessageForm(v, date[i], true);
				}
				break;
			case 'current':
				console.log('current:' + JSON.stringify(date))
				ItemMsessageForm(v, date);
				break;

			default:

				break;

		}
	}

	return Message;

}());


export default {
	Task: Message,
	Send: function(v, task) {

		var msg = {
			title: task.taskUserInfo.userName,
			url: [task.taskUserInfo.headImg],
			message: task.msg.content.text,
			time: task.msg.time,
			count: 0,
			stick: false,
			key: v.sesstion,
			type: task.msg.type,
			len: task.msg.content.length,
			disabled: false
		};

		for (var i = 0; i < v.messages.length; i++) {
			var item = v.messages[i];
			if (item.key == msg.key) {
				v.messages.splice(i, 1);
			}
		}



		var temp = JSON.parse(JSON.stringify(msg));
		temp.title = task.msg.userinfo.username;
		temp.url = [task.msg.userinfo.face];
		var chatmsg = helper.SesstionMsgForm(temp, task.msg.userinfo.uid);
		helper.SetChatMsg(chatmsg, v.sesstion);

		switch (msg.type) {
			case 'img':
				msg.message = '[图片]';
				break;
			case 'voice':
				msg.message = '[语音]';
				break;
			case 'map':
				msg.message = '[位置]';
				break;
			case 'card':
				msg.message = '[信息卡片]';
				break;
			case 'product':
				msg.message = '[产品]';
				break;
			case 'order':
				msg.message = '[订单信息]';
				break;
			case 'redEnvelope':
				msg.message = '[红包]';
				break;
			case 'text':
			default:
				break;
		}

		v.messages.splice(0, 0, msg);
		helper.SetSesstion(v.messages);


	}

};
