export default {
	openNav: true,
	isHide: false,
	sesstion: '0',

	demo: {
		openmenuBar: false
	},
	
	applist:[],

	userKey: '',
	headImg: '',

	islink: false,
	userId: 0,
	heartbeattime: '',
	heartbeatintervaltime: 0,
	clientid: '',

	minHeight: '0',
	winHeight: '0',
	overflow:'', // hidden

	directionStr: '水平',
	horizontal: 'right',
	vertical: 'bottom',
	direction: 'vertical',

	pattern: {
		color: '#7A7E83',
		backgroundColor: '#fff',
		selectedColor: '#007AFF',
		buttonColor: '#007AFF'
	},
	content: [{
		iconPath: '/static/img/face.jpg',
		selectedIconPath: '/static/img/face.jpg',
		text: '健健',
		active: false
	}],
	options: [{
			text: '已读',
			style: {
				backgroundColor: '#C7C6CD'
			}
		},
		{
			text: '删除',
			style: {
				backgroundColor: '#dd524d'
			}
		}
	],
	messages: []
};
