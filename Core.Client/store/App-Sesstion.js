import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		// 当前页面会话
		pageSesstion:'',
		
		// 新会话消息
		newmsg: '',
		
		// 发送消息
		pushmsg: ''
	},
	mutations: {
		setSesstion(state, value){
			state.pageSesstion = value;
		},
		setMsg(state, value) {
			state.newmsg = value;
		},
		chatPushMsg(state, value){
			state.pushmsg = value;
		}
		
	}
	
})

export default store
