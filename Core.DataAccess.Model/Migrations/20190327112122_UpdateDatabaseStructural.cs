﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class UpdateDatabaseStructural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Project_Module_Params");

            migrationBuilder.DropTable(
                name: "Project_Module_Params_Dic");

            migrationBuilder.DropColumn(
                name: "_clientType",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "_messageContentType",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "_messageType",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "sendMessage",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "sendUserKey",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "session",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "_sessionType",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "toUserKey",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "_clientType",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "_messageContentType",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "_messageType",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "sendMessage",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "sendUserKey",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "session",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "_sessionType",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "toUserKey",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "_clientType",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "_messageContentType",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "_messageType",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "sendMessage",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "sendUserKey",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "session",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "_sessionType",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "toUserKey",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "userKeys",
                table: "Project_Module_Group");

            migrationBuilder.AlterColumn<string>(
                name: "userKey",
                table: "Socket_User_Login",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "MsgContext",
                table: "Socket_Offline_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MsgKey",
                table: "Socket_Offline_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MsgContext",
                table: "Socket_Group_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MsgKey",
                table: "Socket_Group_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MsgContext",
                table: "Socket_Backups_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MsgKey",
                table: "Socket_Backups_Message",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Project_Module_Group_User",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<int>(
                name: "Level",
                table: "Project_Module_Group_User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Project_Module_Group",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.CreateTable(
                name: "ProjectUserGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    ProjectToken = table.Column<string>(nullable: true),
                    AddTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectUserGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProjectUserGroupMember",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupKey = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    ProjectToken = table.Column<string>(nullable: true),
                    AddTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectUserGroupMember", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectUserGroup");

            migrationBuilder.DropTable(
                name: "ProjectUserGroupMember");

            migrationBuilder.DropColumn(
                name: "MsgContext",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "MsgKey",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "MsgContext",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "MsgKey",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "MsgContext",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "MsgKey",
                table: "Socket_Backups_Message");

            migrationBuilder.DropColumn(
                name: "Level",
                table: "Project_Module_Group_User");

            migrationBuilder.AlterColumn<long>(
                name: "userKey",
                table: "Socket_User_Login",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "_clientType",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageContentType",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageType",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "sendMessage",
                table: "Socket_Offline_Message",
                unicode: false,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "sendUserKey",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "session",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "_sessionType",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "toUserKey",
                table: "Socket_Offline_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "_clientType",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageContentType",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageType",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "sendMessage",
                table: "Socket_Group_Message",
                unicode: false,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "sendUserKey",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "session",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "_sessionType",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "toUserKey",
                table: "Socket_Group_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "_clientType",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageContentType",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "_messageType",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "sendMessage",
                table: "Socket_Backups_Message",
                unicode: false,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "sendUserKey",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "session",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "_sessionType",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "toUserKey",
                table: "Socket_Backups_Message",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<long>(
                name: "userId",
                table: "Project_Module_Group_User",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "userId",
                table: "Project_Module_Group",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "userKeys",
                table: "Project_Module_Group",
                unicode: false,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Project_Module_Params",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    dic = table.Column<int>(nullable: false),
                    param = table.Column<string>(unicode: false, nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    userId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Params", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_Params_Dic",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    datelist = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false, defaultValueSql: "('')"),
                    userId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Params_Dic", x => x.id);
                });
        }
    }
}
