﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DataAccess.Model.Project.Logs
{
    public partial class Logger
    {
        public int Id { get; set; }

        /// <summary>
        /// 项目Token
        /// </summary>
        public string ProjectToken { get; set; }

        /// <summary>
        /// 主题
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// 日志标识
        /// </summary>
        public string LogFlag { get; set; }

        /// <summary>
        /// 日志分类
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 日志内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? AddTime { get; set; }

        /// <summary>
        /// LogFlag Group
        /// </summary>
        [NotMapped]
        public IEnumerable<object> ChildLoggers { get; set; }

        /// <summary>
        /// child count
        /// </summary>
        [NotMapped]
        public int ChildCount { get; set; }

    }
}
