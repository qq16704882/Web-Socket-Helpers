﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectCustomFormListDate
    {
        public int Id { get; set; }
        public int? CustomFormListKey { get; set; }
        public string ItemDate { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
