﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Framework.EntityExtend
{
    public interface IEntityFrameWorkCacheService
    {
        /// <summary>
        /// 验证缓存项是否存在
        /// </summary>
        /// <param name="bool">缓存是否存在</param>
        /// <param name="T">缓存数据</param>
        /// <returns></returns>
        Tuple<bool,T> Exists<T>(string key);

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresIn">缓存时长</param>
        /// <param name="isSliding">是否滑动过期（如果在过期时间内有操作，则以当前时间点延长过期时间）</param>
        /// <returns></returns>
        bool Add(string key, object value, TimeSpan expiresIn, bool isSliding = false);

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        bool Remove(string key);
    }
}
