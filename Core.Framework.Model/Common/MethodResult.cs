﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// 方法返回值
    /// </summary>
    public class MethodResult
    {
        /// <summary>
        /// [真] 成功完成
        /// [假] 完成失败
        /// </summary>
        public bool IsFinished { get; set; }

        /// <summary>
        /// 结果信息
        /// </summary>
        public string Discription { get; set; }
    }

    public class MethodResult<T>
    {
        /// <summary>
        /// [真] 成功完成
        /// [假] 完成失败
        /// </summary>
        public bool IsFinished { get; set; }

        /// <summary>
        /// 结果信息
        /// </summary>
        public string Discription { get; set; }

        /// <summary>
        /// 结果数据
        /// </summary>
        public T Date { get; set; }
    }
}
