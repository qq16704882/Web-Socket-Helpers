﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{

    /// <summary>
    /// 微信支付信息
    /// </summary>
    public class PayWChatInfo
    {
        /// <summary>
        /// 微信应用ID
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 支付Api服务地址
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// 微信支付商户号
        /// </summary>
        public string MchId { get; set; }

        /// <summary>
        /// API密钥
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// 付款成功后通知地址
        /// </summary>
        public string NotifyUrl { get; set; }
    }
}
