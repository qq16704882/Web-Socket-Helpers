﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 连接客户机信息
    /// </summary>
    public class ClientInfo
    {
        /// <summary>
        /// 用户身份信息
        /// </summary>
        public UserInfo User { get; set; } = new UserInfo();

        /// <summary>
        /// ws token
        /// </summary>
        public string ClientToken { get; set; }

        /// <summary>
        /// 用户订阅主题
        /// </summary>
        public string Template { get; set; } = "WebSocket";

        /// <summary>
        /// 服务器生成 token
        /// [服务器标识]
        /// </summary>
        public string ServiceToken { get; set; }

        /// <summary>
        /// 项目信息
        /// </summary>
        public ProjectInfo Project { get; set; } = null;

        /// <summary>
        /// 最后心跳时间标记
        /// </summary>
        public DateTime EndHeartTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 当前任务数量
        /// </summary>
        public long TaskLength { get; set; }

    }
}
