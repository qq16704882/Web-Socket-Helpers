﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Framework.Model.Common;

namespace Core.Framework.Redis.Queue_Helper
{
    public static class RedisQueueHelperParameter
    {
        /// <summary>
        /// 队列消息
        /// </summary>
        public static readonly string Queue = $"__QDataBase_{CoreStartupHelper.GetConfigValue("Service:Title")}_MessageQuene";

        /// <summary>
        /// 未处理消息
        /// </summary>
        public const string UntreatedQueue = "__QDataBase_Untreated_MessageQuene";

        /// <summary>
        /// 队列服务表
        /// </summary>
        public static readonly string QueueService = $"__QDataBase_QueueService";

        /// <summary>
        /// 服务客户机名称
        /// </summary>
        public static readonly string ServiceClinet = $"__QDataBase_{CoreStartupHelper.GetConfigValue("Service:Title")}_ServiceClinet";

        /// <summary>
        /// 历史消息
        /// </summary>
        public const string History = "__QDataBase_HistoryMessage";

        /// <summary>
        /// 异常消息
        /// </summary>
        public const string Errer = "__QDataBase_ErrerMessageQuene";

        // 群组未发送[放弃]
        // GroupNull = 2,
        // 单聊未发送
        public const string SingleNull = "__QDataBase_SingleNull";

        /// <summary>
        /// 项目用户登陆
        /// </summary>
        public const string ProjectUserLogin = "__QDataBase_ProjectUserLoginInfo";

        /// <summary>
        /// 即时通讯用户登陆
        /// </summary>
        public const string ProjectSocketUserLogin = "__QDataBase_ProjectSocketUserLoginInfo";

        /// <summary>
        /// 项目信息 By UserToken
        /// </summary>
        public const string ProjectInfoByUserToken= "__QDataBase_ProjectInfoByUserToken";

        /// <summary>
        /// websocket 用户登陆
        /// </summary>
        public const string WebSocketByToken = "__QDataBase_WebSocketByToken";


        /// <summary>
        /// Api 调用
        /// </summary>
        public const string ApiClientByToken = "__QDataBase_ApiClientByToken";


        /// <summary>
        /// 项目信息
        /// </summary>
        public const string ApiProjectInfo = "__QDataBase_ApiProjectInfo";


        /// <summary>
        /// 短信验证码
        /// </summary>
        public const string SMSMessageCode = "__SMS_Code_";

        /// <summary>
        /// 短信验证码重复发送验证
        /// </summary>
        public const string SMSMessageCodeRetry = "__SMS_Retry_";
    }
}
