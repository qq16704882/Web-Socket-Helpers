﻿using System.ComponentModel.DataAnnotations;
using static Core.Framework.Util.Attributes;

namespace Core.Framework.Util
{
    public class CustomizeValidationAttribute: ValidationAttribute
    {
        /// <summary>
        /// 验证类型
        /// </summary>
        public IsValidEnum IsValidType { set; get; } = IsValidEnum.init;
    }
}
