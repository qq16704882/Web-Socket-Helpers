﻿using Quartz;

namespace Core.Framework.Util.Common
{
    /// <summary>
    /// 计划任务接口
    /// </summary>
    public interface ITimerJob
    {
        void Execute(IJobExecutionContext context);
    }
}
