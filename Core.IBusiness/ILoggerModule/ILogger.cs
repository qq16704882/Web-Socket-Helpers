﻿using Core.DataAccess.Model.Project.Logs;
using Core.Framework.Model.Common;
using System;
using System.Collections.Generic;

namespace Core.IBusiness.ILoggerModule
{
    /// <summary>
    /// 日志接口
    /// </summary>
    public interface ILog
    {

        #region 添加日志

        /// <summary>
        /// 输出Info日志
        /// </summary>
        void Info<T>(string value, string projectToken);

        /// <summary>
        /// 输出DeBug日志
        /// </summary>
        void DeBug<T>(string value, string projectToken);

        /// <summary>
        /// 输出Warning日志
        /// </summary>
        void Warning<T>(string value, string projectToken);

        /// <summary>
        /// 输出Error日志
        /// </summary>
        void Error<T>(string value, string projectToken);

        /// <summary>
        /// 输出SqlServer日志
        /// </summary>
        void SqlServer<T>(string value, string tag, string projectToken, string logFlag = "");

        /// <summary>
        /// 输出Queue日志
        /// </summary>
        void Queue<T>(string value, string tag, string projectToken, string logFlag = "");

        /// <summary>
        /// 输出Request日志
        /// </summary>
        void Request<T>(string value, string tag, string projectToken, string logFlag = "");

        /// <summary>
        /// 输出Customize日志
        /// </summary>
        void Customize<T>(string tag, string template, string projectToken, string value, string logFlag = "");

        #endregion


        #region 获取日志

        /// <summary>
        /// 获取Request日志
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="projectToken">项目Token</param>
        /// <param name="search">搜索值</param>
        /// <returns></returns>
        IEnumerable<Logger> GetRequestLoggers(Pagination pagination, string projectToken, string search = "");

        /// <summary>
        /// 获取Queue日志
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="search">搜索值</param>
        /// <param name="projectToken">项目Token</param>
        /// <param name="starTime">起始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        IEnumerable<Logger> GetQueueLoggers(Pagination pagination, DateTime starTime, DateTime endTime, string projectToken, string search = "");

        /// <summary>
        /// 获取SqlServer日志
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="projectToken">项目Token</param>
        /// <param name="search">搜索值</param>
        /// <returns></returns>
        IEnumerable<Logger> GetSqlServerLoggers(Pagination pagination, string projectToken, string search = "");

        /// <summary>
        /// 获取日志
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="projectToken">项目Token</param>
        /// <param name="template">消息模板</param>
        /// <param name="starTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="tag">标签</param>
        /// <param name="tagFlag">标签旗帜</param>
        /// <param name="search">搜索值</param>
        /// <returns></returns>
        IEnumerable<Logger> GetLoggers(Pagination pagination, DateTime starTime,  DateTime endTime, string projectToken, string template = "", string tag = "", string tagFlag = "", string search = "");

        /// <summary>
        /// 获取 所有 Template
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetTemplates(string template, string projectToken);

        /// <summary>
        /// 获取 所有 Tag
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetTags(string template, string projectToken);

        #endregion
    }
}
