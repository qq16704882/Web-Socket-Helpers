﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DataAccess.Model.Project.Queue;

namespace Core.IBusiness.ISocketModule
{
    /// <summary>
    /// 群组管理
    /// </summary>
    public interface ISocketGroup
    {

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id">群组关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroup,bool> GetById(int id, string projectToken);

        /// <summary>
        /// 查询用户群组信息列表
        /// </summary>
        /// <param name="userKey">用户关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        Tuple<List<ProjectModuleGroup>, bool> GetByUserKey(string userKey, string projectToken);

        /// <summary>
        /// 查询用户加入的群组
        /// </summary>
        /// <param name="userKey">用户关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        Tuple<List<int>, bool> GetGroupsByUserKey(string userKey, string projectToken);

        /// <summary>
        /// 查询群组成员
        /// </summary>
        /// <param name="id">群组关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        Tuple<List<ProjectModuleGroupUser>, bool> GetGroupUsersByGroupKey(int id, string projectToken);


        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="model">群组模型</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroup, string, bool> Create(ProjectModuleGroup model);


        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">群组模型</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroup, string, bool> Update(ProjectModuleGroup model);

        /// <summary>
        /// 解散
        /// </summary>
        /// <param name="id">群组关键字</param>
        /// <param name="userKey">用户关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroup, bool> Delete(int id, string userKey, string projectToken);

        /// <summary>
        /// 加入群组
        /// </summary>
        /// <param name="model">群组用户模型</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroupUser, string, bool> AddGroupUser(ProjectModuleGroupUser model);


        /// <summary>
        /// 修改组员信息
        /// </summary>
        /// <param name="model">群组用户模型</param>
        /// <returns></returns>
        Tuple<ProjectModuleGroupUser, string, bool> UpdateGroupUser(ProjectModuleGroupUser model);


    }
}
