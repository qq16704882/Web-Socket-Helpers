﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// ws 登陆参数
    /// </summary>
    public class WsParameter
    {
        /// <summary>
        /// 扩展参数
        /// </summary>
        public string Parameter { get; set; }

        /// <summary>
        /// WS 回调地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 主题
        /// </summary>
        public string Template { get; set; }
    }
}
