﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Middleware.WebSockets
{
    /// <summary>
    /// 客户机属性实体
    /// </summary>
    public class QueueService
    {
        /// <summary>
        /// 客户机名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 当前Url地址
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// 当前系统关机密码
        /// </summary>
        public string System { get; set; }

        /// <summary>
        /// 监听列表
        /// </summary>
        public List<Listener_Template> Temps { get; set; }

        /// <summary>
        /// 最后刷时间
        /// </summary>
        public DateTime EndRefreshTime { get; set; }
    }
}
