﻿using System;

namespace Core.Service.TaskHandle
{
    public class ListennerAttribute : Attribute
    {
        /// <summary>
        /// 主题
        /// </summary>
        public string Template { get; set; }
    }
}
