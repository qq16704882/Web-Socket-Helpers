﻿using Core.Framework.Model.WebSockets;

namespace Core.Service.TaskHandle.Task_Model
{
    public class DoWork_Out
    {
        /// <summary>
        /// 是否完成任务
        /// </summary>
        public bool OK { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public object[] Parameters { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public object Message { get; set; }

        /// <summary>
        /// 事件
        /// </summary>
        public string Action { get; set; }


    }
}
