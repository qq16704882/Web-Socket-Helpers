﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient
{
    /// <summary>
    /// 忽略登录
    /// </summary>
    public class IgnoreAuthorizeAttribute : Attribute
    {

    }
}
