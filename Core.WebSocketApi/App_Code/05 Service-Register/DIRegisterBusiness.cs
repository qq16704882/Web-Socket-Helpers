﻿using Core.Business.CommentModule;
using Core.Business.EntrepotModule;
using Core.Business.Information;
using Core.Business.LoggerModule;
using Core.Business.OrderModule;
using Core.Business.ProjectModule;
using Core.Business.SDKService;
using Core.Business.SocketModule;
using Core.Business.UserModule;
using Core.IBusiness.IComment;
using Core.IBusiness.IEntrepotModule;
using Core.IBusiness.IInformation;
using Core.IBusiness.ILoggerModule;
using Core.IBusiness.IOrderModule;
using Core.IBusiness.IProjectModule;
using Core.IBusiness.ISDK;
using Core.IBusiness.ISocketModule;
using Core.IBusiness.IUserModule;
using Microsoft.Extensions.DependencyInjection;

namespace Core.ApiClient
{
    public static class DIRegisterBusiness
    {
        /// <summary>
        /// 注册数据库服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddBusinessService(this IServiceCollection services)
        {
            services.AddSingleton<ILog, Loggers>()

                .AddSingleton<IProject, Project>()
                .AddSingleton<IProjectUser, ProjectUsers>()

                .AddSingleton<IQueue, Queue>()

                .AddSingleton<ISocketGroup, SocketGroup>()
                .AddSingleton<ISocketRelationship, SocketRelationship>()
                .AddSingleton<ISocketMessage, SocketMessage>()


                .AddSingleton<ISocketUser, SocketUsers>();

            return services;
        }
    }
}
