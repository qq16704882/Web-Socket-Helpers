﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess.Model.Project.Queue;
using Core.ApiClient.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.ApiClient.Controllers.Socket
{
    /// <summary>
    /// 用户关系管理
    /// </summary>
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class RelationshipController : BaseApiController
    {
        /// <summary>
        /// 查询用户关系 [分组列表]|[好友列表]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IResult GetAll()
        {
            var result = this.iSocketRelationship.GetAll(iClientInfo.Key.ToString(), iClientInfo.ProjectToken);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = new
                {
                    ProjectUserGroup = result.Item1,
                    ProjectUserGroupMember = result.Item2
                }
            };
        }

        /// <summary>
        /// 查询指定得分组好友
        /// </summary>
        /// <param name="groupKey">分组关键字</param>
        /// <returns></returns>
        [HttpGet(".groupKey{groupKey}")]
        [Description("查询指定得分组好友")]
        public IResult GetGroupMembersByGroupKey(int groupKey)
        {
            var result = this.iSocketRelationship.GetGroupMembersByGroupKey(groupKey, iClientInfo.ProjectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        /// <summary>
        /// 查询指定好友信息
        /// </summary>
        /// <param name="id">关系关键字</param>
        /// <returns></returns>
        [HttpGet(".groupMember{id}")]
        [Description("查询指定好友信息")]
        public IResult GetItemGroupMember(int id)
        {
            var result = this.iSocketRelationship.GetItemGroupMember(id, iClientInfo.ProjectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        /// <summary>
        /// 创建分组
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".group")]
        [Description("创建分组")]
        public IResult CreateGroup(ProjectUserGroup model)
        {
            var result = this.iSocketRelationship.CreateGroup(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        /// <summary>
        /// 加入分组成员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".member")]
        [Description("创建成员")]
        public IResult CreateGroupMember(ProjectUserGroupMember model)
        {
            var result = this.iSocketRelationship.CreateGroupMember(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        /// <summary>
        /// 修改分组
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Description("修改分组")]
        public IResult UpdateGroup(ProjectUserGroup model)
        {
            var result = this.iSocketRelationship.UpdateGroup(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        /// <summary>
        /// 修改分组成员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut(".groupMember")]
        [Description("修改分组成员")]
        public IResult UpdateGroupMember(ProjectUserGroupMember model)
        {
            var result = this.iSocketRelationship.UpdateGroupMember(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        /// <summary>
        /// 删除分组
        /// </summary>
        /// <param name="id">分组关键字</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IResult DeleteGroup(int id)
        {
            var result = this.iSocketRelationship.DeleteGroup(id, iClientInfo.ProjectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        /// <summary>
        /// 删除分组成员
        /// </summary>
        /// <param name="id">关系关键字</param>
        /// <returns></returns>
        [HttpDelete(".member{id}")]
        public IResult DeleteGroupMember(int id)
        {
            var result = this.iSocketRelationship.DeleteGroupMember(id,iClientInfo.ProjectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }
    }
}
