﻿using Core.ApiClient.Controllers.Socket.AcceptModel;
using Core.ApiClient.Model;
using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Model.WebSockets;
using Core.IBusiness.IUserModule.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using Core.Framework.Util;

namespace Core.ApiClient.Controllers.Socket
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class UserController : BaseApiController
    {
        /// <summary>
        /// 获取用户信息根据Token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IResult Get()
        {
            var result = this.iSocketUser.GetUserInfoByToken(iClientInfo.Token, iClientInfo.ProjectToken);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 客户端缓存用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet(".cAll")]
        public IResult GetAll()
        {
            var result = this.iSocketUser.GetUserInfoAll(iClientInfo.ProjectToken);


            List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();

            if (result.IsFinished)
            {
                foreach (var item in result.Date)
                {
                    list.Add(new KeyValuePair<int, string>(item.Id,item.Paras));
                }
            }

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = list
            };
        }



        /// <summary>
        /// 获取用户信息根据关键字
        /// </summary>
        /// <param name="userid">用户关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <returns></returns>
        [HttpGet("{userID}/{projectToken}")]
        [IgnoreAuthorize]
        public IResult GetByUserId(int userid, string projectToken)
        {
            var result = this.iSocketUser.LoginByKey(userid, projectToken, 24 * 3);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 获取用户信息根据关键字并设置缓存
        /// </summary>
        /// <param name="userid">用户关键字</param>
        /// <param name="projectToken">项目Token</param>
        /// <param name="hour">缓存时间/H</param>
        /// <returns></returns>
        [HttpGet("{userID}/{projectToken}.{hour}h")]
        [IgnoreAuthorize]
        public IResult GetByCache(int userid, string projectToken, int hour)
        {
            var result = this.iSocketUser.LoginByKey(userid, projectToken, hour);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [IgnoreAuthorize]
        public IResult Reg(ProjectModuleUser model)
        {

            model.ProjectToken = iProjectInfo.Token;

            var result = this.iSocketUser.Reg(model);
            return new ApiResult
            {
                code = result.Item1 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item2,
                info = result.Item3
            };
        }

        /// <summary>
        /// 根据用户名密码登陆
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost(".user")]
        [IgnoreAuthorize]
        public IResult Login(LoginByUser request)
        {
            request.ApiProjectInfo = iProjectInfo;
            var result = this.iSocketUser.Login(request);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 根据手机号密码登陆
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost(".phone")]
        [IgnoreAuthorize]
        public IResult LoginByPhone(LoginByPhone request)
        {
            request.ApiProjectInfo = iProjectInfo;
            var result = this.iSocketUser.LoginByPhone(request);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 根据UUID登陆
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost(".client")]
        [IgnoreAuthorize]
        public IResult LoginByClient(LoginByUUID request)
        {

            request.ApiProjectInfo = iProjectInfo;

            var result = this.iSocketUser.LoginByClient(request);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }

        /// <summary>
        /// 根据微信OPENID登陆
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost(".wchat")]
        [IgnoreAuthorize]
        public IResult LoginByWChat(LoginByWChat request)
        {
            request.ApiProjectInfo = iProjectInfo;
            var result = this.iSocketUser.LoginByWChat(request);

            return new ApiResult
            {
                code = result.IsFinished ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Date,
                info = result.Discription
            };
        }



        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public IResult UpdateUserInfo(ProjectModuleUser model)
        {
            var result = this.iSocketUser.UpdateUserInfo(model);

            return new ApiResult
            {
                code = model == null ? CodeResult.PARAMS_TYPE_ERROR : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 修改用户扩展参数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut(".paras")]
        public IResult UpdateUserInfoParas(ProjectModuleUser model)
        {
            model.Id = iClientInfo.Key;
            model.Token = iClientInfo.Token;

            var result = this.iSocketUser.UpdateUserInfoParas(model.Id,model.Token,model.ProjectToken,model.Paras);

            return new ApiResult
            {
                code = model == null ? CodeResult.PARAMS_TYPE_ERROR : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 退出登陆根据用户Token
        /// </summary>
        [HttpDelete]
        public void OutLogin()
        {
            this.iSocketUser.OutLogin(iClientInfo.Token, iClientInfo.ProjectToken);
            {
                //WebSocketApplication.ClientsPool
                // 处理 客户机下线
            }
        }

        /// <summary>
        /// 退出SocketClient根据ServiceToken
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete(".wsbyservice{token}")]
        public IResult OutLoginByServiceToken(string token)
        {
            var clients = WebSocketApplication.ClientsPool
                    .Where(a => a.Key.State == WebSocketState.Open && a.Value.ServiceToken == token);

            // 判断是否在线
            if (clients.Count() > 0)
            {
                // 下线
                foreach (var item in clients)
                {
                    WebSocketApplication.ClientLoginOut(item.Key);
                }
            }

            return new ApiResult
            {
                code = CodeResult.SUCCESS
            };
        }

        /// <summary>
        /// 退出SocketClient根据ClientToken
        /// </summary>
        /// <returns></returns>
        [HttpDelete(".ws")]
        public IResult WsOutLogin()
        {
            var clients = WebSocketApplication.ClientsPool
                    .Where(a => a.Key.State == WebSocketState.Open && a.Value.ClientToken == iClientInfo.Token);

            // 判断是否在线
            if (clients.Count() > 0)
            {
                // 下线
                foreach (var item in clients)
                {
                    WebSocketApplication.ClientLoginOut(item.Key);
                }
            }


            return new ApiResult
            {
                code =  CodeResult.SUCCESS
            };

        }

    }
}
